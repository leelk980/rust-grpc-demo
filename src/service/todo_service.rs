use crate::{
    generated::todo::{
        todo_server::Todo, CreateOneTodoData, CreateOneTodoView, FindOneTodoData, FindOneTodoView,
    },
    handler::{create_one_todo::handle_create_one_todo, find_one_todo::handle_find_one_todo},
};

pub struct TodoService {}

#[tonic::async_trait]
impl Todo for TodoService {
    #[must_use]
    #[allow(clippy::type_complexity, clippy::type_repetition_in_bounds)]
    fn create_one_todo<'life0, 'async_trait>(
        &'life0 self,
        request: tonic::Request<CreateOneTodoData>,
    ) -> ::core::pin::Pin<
        Box<
            dyn ::core::future::Future<
                    Output = std::result::Result<tonic::Response<CreateOneTodoView>, tonic::Status>,
                > + ::core::marker::Send
                + 'async_trait,
        >,
    >
    where
        'life0: 'async_trait,
        Self: 'async_trait,
    {
        let view = handle_create_one_todo(request.into_inner());
        Box::pin(async move { Ok(tonic::Response::new(view)) })
    }

    #[must_use]
    #[allow(clippy::type_complexity, clippy::type_repetition_in_bounds)]
    fn find_one_todo<'life0, 'async_trait>(
        &'life0 self,
        request: tonic::Request<FindOneTodoData>,
    ) -> ::core::pin::Pin<
        Box<
            dyn ::core::future::Future<
                    Output = std::result::Result<tonic::Response<FindOneTodoView>, tonic::Status>,
                > + ::core::marker::Send
                + 'async_trait,
        >,
    >
    where
        'life0: 'async_trait,
        Self: 'async_trait,
    {
        let view = handle_find_one_todo(request.into_inner());
        Box::pin(async move { Ok(tonic::Response::new(view)) })
    }
}
