use rust_grpc::{generated::todo::todo_server, service::todo_service::TodoService};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let todo_service = todo_server::TodoServer::new(TodoService {});

    let reflection_service = tonic_reflection::server::Builder::configure()
        .register_encoded_file_descriptor_set(tonic::include_file_descriptor_set!(
            "todo_descriptor"
        ))
        .build()
        .unwrap();

    println!("Server is running on 8000");

    tonic::transport::Server::builder()
        .add_service(todo_service)
        .add_service(reflection_service)
        .serve("127.0.0.1:8000".parse()?)
        .await?;

    Ok(())
}
