use crate::generated::todo::{CreateOneTodoData, CreateOneTodoView};

pub fn handle_create_one_todo(params: CreateOneTodoData) -> CreateOneTodoView {
    println!("content={:?}", params.content);
    CreateOneTodoView { id: 1 }
}
