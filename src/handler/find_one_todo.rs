use crate::generated::todo::{FindOneTodoData, FindOneTodoView};

pub fn handle_find_one_todo(params: FindOneTodoData) -> FindOneTodoView {
    print!("id={:?}", params.id);

    FindOneTodoView {
        id: 1,
        content: "study rust".to_owned(),
        status: "progress".to_owned(),
    }
}
