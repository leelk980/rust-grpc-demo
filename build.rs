use std::{env, path::PathBuf};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    tonic_build::configure()
        .build_client(false)
        .build_server(true)
        .file_descriptor_set_path(out_dir.join("todo_descriptor.bin"))
        .out_dir("src/generated")
        .compile(&["proto/post.proto", "proto/todo.proto"], &["proto"])?;

    Ok(())
}
