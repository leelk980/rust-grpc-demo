import { generatePostProto } from "./post";
import { generateTodoProto } from "./todo";

function main() {
  generatePostProto();
  generateTodoProto();
}

main();
