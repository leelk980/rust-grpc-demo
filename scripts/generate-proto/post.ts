import * as fs from "fs";
import typia, { tags } from "typia";

interface CreateOnePostData {
  title: string & tags.MinLength<1>;
  content: string & tags.MinLength<1>;
}

interface CreateOnePostView {
  id: number & tags.Type<"int32">;
}

interface FindOnePostData {
  id: number & tags.Type<"int32">;
}

interface FindOnePostView {
  id: number & tags.Type<"int32">;
  title: string & tags.MinLength<1>;
  content: string & tags.MinLength<1>;
}

export function generatePostProto() {
  const packageName = "post";
  const serviceStr = `
// this file is generated, do not modify manually.
syntax = "proto2";

package ${packageName};

service ${packageName.charAt(0).toUpperCase() + packageName.slice(1)} {
  rpc createOneTodo(CreateOnePostData) returns (CreateOnePostView);

  rpc findOneTodo(FindOnePostData) returns (FindOnePostView);
}
`.trim();

  const syntaxStr = 'syntax = "proto3";';

  return fs.promises.writeFile(
    `proto/${packageName}.proto`,
    serviceStr +
      typia.protobuf.message<CreateOnePostData>().split(syntaxStr)[1] +
      typia.protobuf.message<CreateOnePostView>().split(syntaxStr)[1] +
      typia.protobuf.message<FindOnePostData>().split(syntaxStr)[1] +
      typia.protobuf.message<FindOnePostView>().split(syntaxStr)[1] +
      ""
  );
}
