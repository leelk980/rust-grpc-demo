import * as fs from "fs";
import typia, { tags } from "typia";

interface CreateOneTodoData {
  content: string & tags.MinLength<1>;
  status: "ready" | "progress" | "done";
}

interface CreateOneTodoView {
  id: number & tags.Type<"int32">;
}

interface FindOneTodoData {
  id: number & tags.Type<"int32">;
}

interface FindOneTodoView {
  id: number & tags.Type<"int32">;
  content: string & tags.MinLength<1>;
  status: "ready" | "progress" | "done";
}

export function generateTodoProto() {
  const packageName = "todo";
  const serviceStr = `
// this file is generated, do not modify manually.
syntax = "proto2";

package ${packageName};
  
service ${packageName.charAt(0).toUpperCase() + packageName.slice(1)} {
  rpc createOneTodo(CreateOneTodoData) returns (CreateOneTodoView);

  rpc findOneTodo(FindOneTodoData) returns (FindOneTodoView);
}
`.trim();

  const syntaxStr = 'syntax = "proto3";';

  return fs.promises.writeFile(
    "proto/todo.proto",
    serviceStr +
      typia.protobuf.message<CreateOneTodoData>().split(syntaxStr)[1] +
      typia.protobuf.message<CreateOneTodoView>().split(syntaxStr)[1] +
      typia.protobuf.message<FindOneTodoData>().split(syntaxStr)[1] +
      typia.protobuf.message<FindOneTodoView>().split(syntaxStr)[1] +
      ""
  );
}
