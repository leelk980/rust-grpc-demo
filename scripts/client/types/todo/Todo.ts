// Original file: proto/todo.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { CreateOneTodoData as _todo_CreateOneTodoData, CreateOneTodoData__Output as _todo_CreateOneTodoData__Output } from '../todo/CreateOneTodoData';
import type { CreateOneTodoView as _todo_CreateOneTodoView, CreateOneTodoView__Output as _todo_CreateOneTodoView__Output } from '../todo/CreateOneTodoView';
import type { FindOneTodoData as _todo_FindOneTodoData, FindOneTodoData__Output as _todo_FindOneTodoData__Output } from '../todo/FindOneTodoData';
import type { FindOneTodoView as _todo_FindOneTodoView, FindOneTodoView__Output as _todo_FindOneTodoView__Output } from '../todo/FindOneTodoView';

export interface TodoClient extends grpc.Client {
  createOneTodo(argument: _todo_CreateOneTodoData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, metadata: grpc.Metadata, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, metadata: grpc.Metadata, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _todo_CreateOneTodoData, callback: grpc.requestCallback<_todo_CreateOneTodoView__Output>): grpc.ClientUnaryCall;
  
  findOneTodo(argument: _todo_FindOneTodoData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, metadata: grpc.Metadata, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, metadata: grpc.Metadata, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, options: grpc.CallOptions, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _todo_FindOneTodoData, callback: grpc.requestCallback<_todo_FindOneTodoView__Output>): grpc.ClientUnaryCall;
  
}

export interface TodoHandlers extends grpc.UntypedServiceImplementation {
  createOneTodo: grpc.handleUnaryCall<_todo_CreateOneTodoData__Output, _todo_CreateOneTodoView>;
  
  findOneTodo: grpc.handleUnaryCall<_todo_FindOneTodoData__Output, _todo_FindOneTodoView>;
  
}

export interface TodoDefinition extends grpc.ServiceDefinition {
  createOneTodo: MethodDefinition<_todo_CreateOneTodoData, _todo_CreateOneTodoView, _todo_CreateOneTodoData__Output, _todo_CreateOneTodoView__Output>
  findOneTodo: MethodDefinition<_todo_FindOneTodoData, _todo_FindOneTodoView, _todo_FindOneTodoData__Output, _todo_FindOneTodoView__Output>
}
