// Original file: proto/todo.proto


export interface FindOneTodoView {
  'id'?: (number);
  'content'?: (string);
  'status'?: (string);
}

export interface FindOneTodoView__Output {
  'id': (number);
  'content': (string);
  'status': (string);
}
