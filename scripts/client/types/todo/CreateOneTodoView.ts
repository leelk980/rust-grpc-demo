// Original file: proto/todo.proto


export interface CreateOneTodoView {
  'id'?: (number);
}

export interface CreateOneTodoView__Output {
  'id': (number);
}
