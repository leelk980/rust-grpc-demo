// Original file: proto/todo.proto


export interface FindOneTodoData {
  'id'?: (number);
}

export interface FindOneTodoData__Output {
  'id': (number);
}
