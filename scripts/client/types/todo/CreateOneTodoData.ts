// Original file: proto/todo.proto


export interface CreateOneTodoData {
  'content'?: (string);
  'status'?: (string);
}

export interface CreateOneTodoData__Output {
  'content': (string);
  'status': (string);
}
