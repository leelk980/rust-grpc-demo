// Original file: proto/post.proto


export interface FindOnePostData {
  'id'?: (number);
}

export interface FindOnePostData__Output {
  'id': (number);
}
