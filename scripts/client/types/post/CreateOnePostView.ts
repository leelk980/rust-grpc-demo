// Original file: proto/post.proto


export interface CreateOnePostView {
  'id'?: (number);
}

export interface CreateOnePostView__Output {
  'id': (number);
}
