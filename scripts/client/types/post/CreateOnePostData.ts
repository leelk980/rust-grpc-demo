// Original file: proto/post.proto


export interface CreateOnePostData {
  'title'?: (string);
  'content'?: (string);
}

export interface CreateOnePostData__Output {
  'title': (string);
  'content': (string);
}
