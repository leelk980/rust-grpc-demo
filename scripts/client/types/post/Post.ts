// Original file: proto/post.proto

import type * as grpc from '@grpc/grpc-js'
import type { MethodDefinition } from '@grpc/proto-loader'
import type { CreateOnePostData as _post_CreateOnePostData, CreateOnePostData__Output as _post_CreateOnePostData__Output } from '../post/CreateOnePostData';
import type { CreateOnePostView as _post_CreateOnePostView, CreateOnePostView__Output as _post_CreateOnePostView__Output } from '../post/CreateOnePostView';
import type { FindOnePostData as _post_FindOnePostData, FindOnePostData__Output as _post_FindOnePostData__Output } from '../post/FindOnePostData';
import type { FindOnePostView as _post_FindOnePostView, FindOnePostView__Output as _post_FindOnePostView__Output } from '../post/FindOnePostView';

export interface PostClient extends grpc.Client {
  createOneTodo(argument: _post_CreateOnePostData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, metadata: grpc.Metadata, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, options: grpc.CallOptions, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, metadata: grpc.Metadata, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, options: grpc.CallOptions, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  createOneTodo(argument: _post_CreateOnePostData, callback: grpc.requestCallback<_post_CreateOnePostView__Output>): grpc.ClientUnaryCall;
  
  findOneTodo(argument: _post_FindOnePostData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, metadata: grpc.Metadata, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, options: grpc.CallOptions, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, metadata: grpc.Metadata, options: grpc.CallOptions, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, metadata: grpc.Metadata, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, options: grpc.CallOptions, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  findOneTodo(argument: _post_FindOnePostData, callback: grpc.requestCallback<_post_FindOnePostView__Output>): grpc.ClientUnaryCall;
  
}

export interface PostHandlers extends grpc.UntypedServiceImplementation {
  createOneTodo: grpc.handleUnaryCall<_post_CreateOnePostData__Output, _post_CreateOnePostView>;
  
  findOneTodo: grpc.handleUnaryCall<_post_FindOnePostData__Output, _post_FindOnePostView>;
  
}

export interface PostDefinition extends grpc.ServiceDefinition {
  createOneTodo: MethodDefinition<_post_CreateOnePostData, _post_CreateOnePostView, _post_CreateOnePostData__Output, _post_CreateOnePostView__Output>
  findOneTodo: MethodDefinition<_post_FindOnePostData, _post_FindOnePostView, _post_FindOnePostData__Output, _post_FindOnePostView__Output>
}
