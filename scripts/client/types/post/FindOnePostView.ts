// Original file: proto/post.proto


export interface FindOnePostView {
  'id'?: (number);
  'title'?: (string);
  'content'?: (string);
}

export interface FindOnePostView__Output {
  'id': (number);
  'title': (string);
  'content': (string);
}
