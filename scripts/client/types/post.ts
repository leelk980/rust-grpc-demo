import type * as grpc from '@grpc/grpc-js';
import type { MessageTypeDefinition } from '@grpc/proto-loader';

import type { PostClient as _post_PostClient, PostDefinition as _post_PostDefinition } from './post/Post';

type SubtypeConstructor<Constructor extends new (...args: any) => any, Subtype> = {
  new(...args: ConstructorParameters<Constructor>): Subtype;
};

export interface ProtoGrpcType {
  post: {
    CreateOnePostData: MessageTypeDefinition
    CreateOnePostView: MessageTypeDefinition
    FindOnePostData: MessageTypeDefinition
    FindOnePostView: MessageTypeDefinition
    Post: SubtypeConstructor<typeof grpc.Client, _post_PostClient> & { service: _post_PostDefinition }
  }
}

