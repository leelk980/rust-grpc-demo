import { promisify } from "util";
import { todoClient } from "./todo";

const fnCacheMap: Record<string, Function> = {};

const promisifyGrpc =
  <T extends object>(client: T) =>
  <K extends keyof T>(methodName: K) =>
  (
    request: T[K] extends (...params: any[]) => any
      ? Parameters<T[K]>[0]
      : never
  ) => {
    const cachedFn = fnCacheMap?.[methodName.toString()];
    if (cachedFn) {
      return cachedFn(request);
    }

    const fn = promisify((client[methodName] as any).bind(client));

    fnCacheMap[methodName.toString()] = fn;

    return fn(request);
  };

async function main() {
  const res = await promisifyGrpc(todoClient)("findOneTodo")({ id: 1 });
  console.log(res);

  const res2 = await promisifyGrpc(todoClient)("findOneTodo")({ id: 1 });
  console.log(res2);

  const res3 = await promisifyGrpc(todoClient)("findOneTodo")({ id: 1 });
  console.log(res3);
}

main();
