import * as grpc from "@grpc/grpc-js";
import * as protoLoader from "@grpc/proto-loader";
import { ProtoGrpcType } from "./types/post";

// Load the generated proto files
const packageDefinition = protoLoader.loadSync("proto/post.proto", {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const grpcObject = grpc.loadPackageDefinition(
  packageDefinition
) as unknown as ProtoGrpcType;

export const postClient = new grpcObject.post.Post(
  "localhost:8000",
  grpc.credentials.createInsecure()
);
