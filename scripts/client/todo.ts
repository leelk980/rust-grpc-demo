import * as grpc from "@grpc/grpc-js";
import * as protoLoader from "@grpc/proto-loader";
import { ProtoGrpcType } from "./types/todo";

// Load the generated proto files
const packageDefinition = protoLoader.loadSync("proto/todo.proto", {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const grpcObject = grpc.loadPackageDefinition(
  packageDefinition
) as unknown as ProtoGrpcType;

export const todoClient = new grpcObject.todo.Todo(
  "localhost:8000",
  grpc.credentials.createInsecure()
);
